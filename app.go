package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/queue"
	"github.com/gocolly/colly/proxy"
	"os"
	"bufio"
	"regexp"
	"time"
	"crypto/md5"
	"encoding/hex"
	"strings"
	"sync"
	"hash/crc64"
	"compress/gzip"
	"net/http"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	dto "github.com/prometheus/client_model/go"
	"github.com/op/go-logging"
	"log"
	"context"
	"github.com/go-kivik/kivik"
	_ "github.com/go-kivik/couchdb" // The CouchDB driver
	"os/signal"
	"syscall"
	"strconv"
)

type SaveUrlFilter struct {
	RegExp         string
	KeywordPresent string
}

type Configuration struct {
	DefaultScheme   string
	DefaultDomain   string
	StartingUrls    []string
	ProxyListPath   string
	ReadUrlFilters  []string
	SaveUrlFilters  []SaveUrlFilter
	RequestDelayMs  int
	Threads         int
	IgnoreRobotsTxt bool
	UserAgent       string
	LogLevel        string

	CouchDbScheme   string
	CouchDbHost     string
	CouchDbPort     int16
	CouchDbName     string
	CouchDbUser     string
	CouchDbPassword string

	PrometheusSrvStart bool
	PrometheusSrvPort  int
	PrometheusSrvUrl   string
}

type DbDoc struct {
	ID     string `json:"_id"`
	Rev    string `json:"_rev,omitempty"`
	Source string `json:"source"`
	Scheme string `json:"scheme"`
}

var (
	statsScrapePeriod = 5

	statsLinksParsed = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "links_parsed_total",
		Help: "Total number of parsed links",
	})

	statsLinksEnqueued = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "links_enqueued_total",
		Help: "Total number of enqueued links",
	})

	statsLinksDiscarded = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "links_discarded_total",
		Help: "Total number of discarded links",
	})

	statsLinksFollowed = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "links_followed_total",
		Help: "Number of links followed by scraper",
	})

	statsPagesDiscarded = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "pages_discarded_total",
		Help: "How many pages were checked and discarded," +
			" as they do not match save regular expressions",
	})

	statsPagesSkipped = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "pages_skipped_total",
		Help: "Number of pages that were skipped because already saved",
	})

	statsPagesSaved = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "pages_saved_total",
		Help: "Number of pages saved to the file system",
	})

	statsRequestsSent = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Number of requests sent",
	})

	statsResponsesErrors = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "http_errors_total",
		Help: "Number of errors in response",
	})

	statsResponsesReceived = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "http_responses_total",
		Help: "Number of responses received",
	})

	statsQueueSize = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "queue_current_size",
		Help: "Queue current size",
	})

	gologger = logging.MustGetLogger("scraper")

	format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
	)
)

const (
	DiscardUrlNoPath            = 1
	DiscardUrlJsPresent         = 2
	DiscardUrlReadRegexMismatch = 3
	DiscardUrlNoDomain          = 4
	DiscardUrlAlreadySaved      = 5
)

func init() {
	prometheus.MustRegister(statsLinksParsed)
	prometheus.MustRegister(statsLinksEnqueued)
	prometheus.MustRegister(statsLinksDiscarded)
	prometheus.MustRegister(statsLinksFollowed)
	prometheus.MustRegister(statsPagesDiscarded)
	prometheus.MustRegister(statsPagesSkipped)
	prometheus.MustRegister(statsPagesSaved)
	prometheus.MustRegister(statsRequestsSent)
	prometheus.MustRegister(statsResponsesErrors)
	prometheus.MustRegister(statsResponsesReceived)
	prometheus.MustRegister(statsQueueSize)
}

func panicIfError(e error) {
	if e != nil {
		panic(e)
	}
}

func writeFileGZ(filename string, b []byte) {
	// Create GZ file
	fi, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	panicIfError(err)

	gf := gzip.NewWriter(fi)
	fw := bufio.NewWriter(gf)

	_, err = fw.Write(b)
	panicIfError(err)

	err = fw.Flush()
	panicIfError(err)

	// Close the gzip first.
	err = gf.Close()
	panicIfError(err)

	err = fi.Close()
	panicIfError(err)
}

func validateConfig(Config Configuration) (bool, []string) {
	var errors []string

	schemes := []string{"http://", "https://"}
	validScheme := false

	for _, s := range schemes {
		if s == Config.DefaultScheme {
			validScheme = true
		}
	}

	if validScheme == false {
		errors = append(errors, "Invalid schema provided")
	}

	if len(Config.StartingUrls) == 0 {
		errors = append(errors, "StartingUrls parameter should not be empty")
	}

	if len(Config.SaveUrlFilters) == 0 {
		errors = append(errors, "SaveUrlFilters parameter should not be empty")
	}

	if len(Config.LogLevel) == 0 {
		errors = append(
			errors,
			"LogLevel parameter should not be empty. Use one of:",
			"CRITICAL",
			"ERROR",
			"WARNING",
			"NOTICE",
			"INFO",
			"DEBUG",
		)
	}

	if len(errors) > 0 {
		return false, errors
	}

	return true, errors
}

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func startPrometheusServer(port int, urlPattern string) *http.Server {
	srv := &http.Server{Addr: ":" + strconv.Itoa(port)}

	http.Handle(urlPattern, promhttp.Handler())

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			// cannot panic, because this probably is an intentional close
			log.Printf("Httpserver: ListenAndServe() error: %s", err)
		}
	}()

	// returning reference so caller can call Shutdown()
	return srv
}

func updateQueueSize(q *queue.Queue, g prometheus.Gauge) {
	s := 0
	s, _ = q.Size()
	g.Set(float64(s))
}

func startUpdatingQueueSize(q *queue.Queue, g prometheus.Gauge) {
	p := time.Duration(statsScrapePeriod)
	for {
		<-time.After(p * time.Second)
		go updateQueueSize(q, g)
	}
}

func readCounter(m prometheus.Counter) float64 {
	pb := &dto.Metric{}
	m.Write(pb)
	return pb.GetCounter().GetValue()
}

func removeLock(lockFile string) {
	err := os.Remove(lockFile)

	if err != nil {
		gologger.Criticalf("Could not delete lock file %s", lockFile)
	}
}

func main() {
	timeStart := time.Now().Unix()

	configFile := flag.String("c", "config.conf", "Specify the configuration file.")

	flag.Parse()

	lockFile := "lock." + *configFile

	if _, err := os.Stat(lockFile); ! os.IsNotExist(err) {
		fmt.Println(
			"An instance of the application is already " +
				"launched using this configuration file with such name.",
		)
		os.Exit(0);
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT)
	signal.Notify(signals, syscall.SIGTERM)

	os.OpenFile(lockFile, os.O_RDONLY|os.O_CREATE, 0666)

	go func() {
		<-signals

		// Giving time to finish another processes
		time.Sleep(time.Second)

		removeLock(lockFile)

		os.Exit(0);
	}()

	file, err := os.Open(*configFile)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer file.Close()

	decoder := json.NewDecoder(file)

	Config := Configuration{}

	err = decoder.Decode(&Config)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	isValidConfig, validationErrors := validateConfig(Config)

	if !isValidConfig {
		fmt.Println("Config validation errors:")

		for _, e := range validationErrors {
			fmt.Println(e)
		}

		os.Exit(1)
	}

	ll, err := logging.LogLevel(Config.LogLevel)
	panicIfError(err)

	var logBackendBase = logging.NewLogBackend(os.Stderr, "", 0)
	var logBackendFormatted = logging.NewBackendFormatter(logBackendBase, format)
	var logBackend = logging.AddModuleLevel(logBackendFormatted)
	logBackend.SetLevel(ll, "")
	logging.SetBackend(logBackend)

	var cOpts []func(*colly.Collector)

	if Config.IgnoreRobotsTxt {
		cOpts = append(cOpts, colly.IgnoreRobotsTxt())
	}

	if len(Config.UserAgent) > 0 {
		cOpts = append(cOpts, colly.UserAgent(Config.UserAgent))
	}

	dataSourceName := fmt.Sprintf(
		"%s://%s:%s@%s:%d/",
		Config.CouchDbScheme,
		Config.CouchDbUser,
		Config.CouchDbPassword,
		Config.CouchDbHost,
		Config.CouchDbPort,
	)

	dataSourceSafe := fmt.Sprintf(
		"%s://%s@%s:%d/",
		Config.CouchDbScheme,
		Config.CouchDbUser,
		Config.CouchDbHost,
		Config.CouchDbPort,
	)

	gologger.Debugf("Connecting to database %s...", dataSourceSafe)
	client, err := kivik.New(context.TODO(), "couch", dataSourceName)
	panicIfError(err)
	gologger.Debug("Done")

	var db *kivik.DB
	gologger.Debug("Checking if database exists...")
	if exists, err := client.DBExists(context.TODO(), Config.CouchDbName); exists == true && err == nil {
		gologger.Debug("Database exists. Connecting...")
		db, err = client.DB(context.TODO(), Config.CouchDbName)
	} else {
		gologger.Debug("Database does not exist. Creating and connecting...")
		db, err = client.CreateDB(context.TODO(), Config.CouchDbName)
	}
	panicIfError(err)
	gologger.Debug("Done")

	c := colly.NewCollector(cOpts...)

	if len(Config.ProxyListPath) > 0 {
		gologger.Info("Using proxies")

		proxies, err := readLines(Config.ProxyListPath)

		if err != nil {
			gologger.Errorf("Error occurred while reading proxy list: %s", err)
			os.Exit(1)
		}

		rp, err := proxy.RoundRobinProxySwitcher(proxies...)
		if err != nil {
			gologger.Error(err)
			os.Exit(1)
		}
		c.SetProxyFunc(rp)
	}

	gologger.Info("Creating queue...")
	q, _ := queue.New(
		Config.Threads,                                  // Number of consumer threads
		&queue.InMemoryQueueStorage{MaxSize: 100000000}, // Use default queue storage
	)
	go startUpdatingQueueSize(q, statsQueueSize)
	gologger.Info("Done")

	gologger.Info("Configuring collector...")
	var crc64Polynome uint64 = 0xC96C5795D7870F42 // @see https://en.wikipedia.org/wiki/Cyclic_redundancy_check
	var crc64Table = crc64.MakeTable(crc64Polynome)
	var cache = sync.Map{}

	c.OnRequest(func(r *colly.Request) {
		statsRequestsSent.Inc()
	})

	c.OnResponse(func(r *colly.Response) {
		statsResponsesReceived.Inc()
	})

	c.OnError(func(r *colly.Response, e error) {
		statsResponsesErrors.Inc()
	})

	var readRegexps []*regexp.Regexp
	for _, f := range Config.ReadUrlFilters {
		readRegexps = append(readRegexps, regexp.MustCompile(f))
	}

	// Find and check all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		statsLinksParsed.Inc()
		href := e.Attr("href")
		url := ""

		gologger.Debugf("Found href: %s", href)

		// Check: href starts with hashtag
		if len(href) == 0 || strings.HasPrefix(href, "#") {
			statsLinksDiscarded.Inc()
			gologger.Debugf("Discarded (%d): %s", DiscardUrlNoPath, href)
			return
		}

		if strings.Contains(href, "javascript:") {
			gologger.Debugf("Discarded (%d): %s", DiscardUrlJsPresent, href)
			statsLinksDiscarded.Inc()
			return
		}

		switch true {
		case strings.HasPrefix(href, "//"):
			url = Config.DefaultScheme + Config.DefaultDomain + href[2:]
			break
		case strings.HasPrefix(href, "/"):
			url = Config.DefaultScheme + Config.DefaultDomain + href
			break
		case ! strings.HasPrefix(href, "http"):
			url = Config.DefaultScheme + Config.DefaultDomain + "/" + href
			break
		}

		if url == "" {
			url = href
		}

		// Truncating all after hashtag and question mark
		var hashTagPos = strings.Index(url, "#")
		var questionMarkPos = strings.Index(url, "?")

		if hashTagPos != -1 {
			url = url[:hashTagPos]
		}

		if questionMarkPos != -1 {
			url = url[:questionMarkPos]
		}

		gologger.Debugf("URL: %s", url)

		skip := true

		for _, r := range readRegexps {
			if r.MatchString(url) {
				skip = false
				break
			}
		}

		if true == skip {
			gologger.Debugf("Discarded (%d): %s", DiscardUrlReadRegexMismatch, url)
			statsPagesDiscarded.Inc()
			return
		}

		// Url does not contain default domain
		if ! strings.Contains(url, Config.DefaultDomain) {
			gologger.Debugf("Discarded (%d): %s", DiscardUrlNoDomain, url)
			statsLinksDiscarded.Inc()
			return
		}

		urlBytes := []byte(url)

		crc := crc64.Checksum(urlBytes, crc64Table)

		gologger.Debugf("CRC: %x", crc)

		if _, loaded := cache.LoadOrStore(crc, true); loaded == true {
			gologger.Debugf("Discarded (%d): %s", DiscardUrlAlreadySaved, url)
			statsLinksDiscarded.Inc()
			return
		}

		newReq, err := e.Request.New("GET", url, nil)
		panicIfError(err)

		q.AddRequest(newReq)
		gologger.Debugf("Enqueued: %s", url)
		statsLinksEnqueued.Inc()
	})

	var saveRegExpToKeyword = make(map[*regexp.Regexp]string)
	for _, f := range Config.SaveUrlFilters {
		r := regexp.MustCompile(f.RegExp)
		saveRegExpToKeyword[r] = f.KeywordPresent
	}

	var sleepDuration time.Duration = 0
	if Config.RequestDelayMs > 0 {
		sleepDuration = time.Duration(Config.RequestDelayMs) * time.Millisecond
	}

	c.OnScraped(func(r *colly.Response) {
		if Config.RequestDelayMs > 0 {
			gologger.Debugf("Asleep for %dms...", Config.RequestDelayMs)
			time.Sleep(sleepDuration)
			gologger.Debug("Awaking")
		}

		r.Headers.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
		r.Headers.Set("Accept-Encoding", "gzip, deflate, br")
		r.Headers.Set("Accept-Language", "RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
		r.Headers.Set("Cache-Control", "max-age=0")
		r.Headers.Set("DNT", "1") // Do Not Track header
		r.Headers.Set("Host", Config.DefaultDomain)
		r.Headers.Set("Referer", Config.DefaultScheme+Config.DefaultDomain+"/")
	})

	// On every a HTML element which has name attribute call callback
	c.OnHTML("html", func(e *colly.HTMLElement) {
		statsLinksFollowed.Inc()

		url := Config.DefaultScheme + Config.DefaultDomain + e.Request.URL.RequestURI()
		urlBytes := []byte(url)

		var resBodyStr = string(e.Response.Body[:]);
		for regex, keyword := range saveRegExpToKeyword {
			if ! regex.MatchString(url) {
				gologger.Debugf("Save regex mismatch: %s", url)
				statsPagesDiscarded.Inc()
				return
			}

			if ! strings.Contains(resBodyStr, keyword) {
				gologger.Debugf("Keyword missing: '%s', %s", keyword, url)
				statsPagesDiscarded.Inc()
				return
			}
		}

		urlHashBytes := md5.Sum(urlBytes)
		urlHashString := hex.EncodeToString(urlHashBytes[:])

		size, _, _ := db.GetMeta(context.TODO(), urlHashString)

		if size != 0 {
			gologger.Infof("Already saved: %s", url)
			statsPagesSkipped.Inc()
			return
		}

		gologger.Infof("Saving: %s", url)
		doc := DbDoc{ID: urlHashString, Source: resBodyStr, Scheme: ""}
		_, err = db.Put(context.TODO(), urlHashString, doc)
		panicIfError(err)
		statsPagesSaved.Inc()
	})

	gologger.Info("Done")

	for _, url := range Config.StartingUrls {
		gologger.Debugf("Adding starting url: %s", url)
		q.AddURL(url)

		crc := crc64.Checksum([]byte(url), crc64Table)
		cache.LoadOrStore(crc, true)
	}

	var srv *http.Server
	gologger.Infof("Starting prometheus server: %t", Config.PrometheusSrvStart)
	if Config.PrometheusSrvStart == true {
		srv = startPrometheusServer(Config.PrometheusSrvPort, Config.PrometheusSrvUrl)
	}

	gologger.Info("Starting collector...")
	q.Run(c)

	timeEnd := time.Now().Unix()

	gologger.Infof("Total links: %.0f\n", readCounter(statsLinksParsed))
	gologger.Infof("Links enqueued: %.0f\n", readCounter(statsLinksEnqueued))
	gologger.Infof("Links discarded: %.0f\n", readCounter(statsLinksDiscarded))
	gologger.Infof("Links followed: %.0f\n", readCounter(statsLinksFollowed))

	gologger.Infof("Pages discarded: %.0f\n", readCounter(statsPagesDiscarded))
	gologger.Infof("Pages skipped: %.0f\n", readCounter(statsPagesSkipped))
	gologger.Infof("Pages saved: %.0f\n", readCounter(statsPagesSaved))

	gologger.Infof("Requests sent: %.0f\n", readCounter(statsRequestsSent))
	gologger.Infof("Responses received: %.0f\n", readCounter(statsResponsesReceived))
	gologger.Infof("Responses errors: %.0f\n", readCounter(statsResponsesErrors))

	gologger.Infof("Threads used: %d\n", Config.Threads)
	gologger.Infof("Duration: %d\n", timeEnd-timeStart)

	if Config.PrometheusSrvStart == true {
		// now close the server gracefully ("shutdown")
		// timeout could be given instead of nil as a https://golang.org/pkg/context/
		if err := srv.Shutdown(nil); err != nil {
			panic(err) // failure/timeout shutting down the server gracefully
		}
	}

	removeLock(lockFile)
}
