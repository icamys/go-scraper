FROM golang:1.10.2 as builder
WORKDIR /app
COPY app.go .

RUN apt-get update \
    && apt-get install ca-certificates

# install all dependencies
RUN go get -d ./... \
    && touch config.json \
    && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

EXPOSE 8080

CMD ["go", "version"]
