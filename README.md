### Usage ###

```
cp config.sample.json config.json
vim config.json

cp proxies.sample.txt proxies.txt
vim proxies.txt

./build/scraper -c config.json
```

### Development ###

```
cp .env.sample .env
vim .env

docker-compose down && docker-compose build && docker-compose up

docker run \
    -v $PWD/config.json:/app/config.json \
    -v $PWD/proxies.txt:/app/proxies.txt \
    --link go-scraper_couchdb_1 \
    go-scraper_go-scraper ./app -c config.json
```

#### Monitoring ####

Prometheus:

```
http://127.0.0.1:9090
```

Grafana:

```
http://127.0.0.1:3000/
```

Local couchdb accessible at: [http://localhost:5984/_utils/#login](http://localhost:5984/_utils/#login)

#### Issues ####

Install default databases in couchdb after first couchdb start:

    docker exec -it go-scraper_couchdb_1 bash
    HOST="http://admin:password@127.0.0.1:5984"
    curl -X PUT $HOST/_users
    curl -X PUT $HOST/_replicator
    curl -X PUT $HOST/_global_changes